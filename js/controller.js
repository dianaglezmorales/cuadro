templateApp.controller('generalController', function ($scope, $timeout) {

	// ---------------------------------------------------------------------- DEFINICION
	$scope.matrix = [
		[2,8,3],
		[1,6,4],
		[7,0,5]
	];
	// $scope.meta = [
	// 	[1,2,3],
	// 	[8,0,4],
	// 	[7,6,5]
	// ];
	// $scope.meta = [
	// 	[2,8,3],
	// 	[1,5,6],
	// 	[7,4,0]
	// ];
	$scope.meta = [
		[1,2,3],
		[8,0,4],
		[7,6,5]
	];
	$scope.verify = function(){
		// console.log($scope.records);
		// console.log($scope.routes);
	}
	var identifyHole = function(m){
		for(my=0; my<m.length; my++) {
			for(mx=0; mx<m.length; mx++) { if(m[my][mx]==0) return {'y':my,'x':mx}; };
		};
	}
	$scope.max = 8;
	var posHole = {};
	var tree = {};
	// ---------------------------------------------------------------------- CREACION
	
	$scope.create = function() {
		tree = { level:0, data:[], child:[], node:0, visited:false, father:{}, cost:0, costs:0, coinc:0 };
		angular.copy($scope.matrix,tree.data);
		$scope.records = [];
		$scope.records.push(tree);
		$scope.routes = [];
		$scope.way = [];
		$scope.clon = [];
		$scope.fin = false;
		posHole = identifyHole($scope.matrix);
		moveHole(posHole.y,posHole.x,tree);
		alert('Árbol Creado');
	}
	var moveHole = function(y,x,node) {
		if( node.level < $scope.max ){
			if( node.data[y-1] != undefined ) addRoute(y,x,y-1,x,node);
			if( node.data[y+1] != undefined ) addRoute(y,x,y+1,x,node);
			if( node.data[y][x-1] != undefined ) addRoute(y,x,y,x-1,node);
			if( node.data[y][x+1] != undefined ) addRoute(y,x,y,x+1,node);
		}
	}
	var addRoute = function(y,x,ny,nx,node){

		var n = { level:0, data:[], child:[], node:$scope.records.length, visited:false, father:node, cost:0, costs:0, coinc:0 }
		angular.copy(node.data,n.data);
		n.data[y][x] = n.data[ny][nx];
		n.data[ny][nx] = 0;
		n.level = node.level+1;

		for(my=0; my<$scope.meta.length; my++) {
			for(mx=0; mx<$scope.meta.length; mx++) { if($scope.meta[my][mx]==n.data[my][mx]) n.coinc++; }
		};
		// n.cost = Math.floor(Math.random()*3)+1;
		// n.costs = node.costs + n.cost;
		n.cost = node.cost + n.coinc;
		n.costs = node.coinc + n.coinc;

		if( !angular.equals(n.data , n.father.father.data )){
			equal=false;
			angular.forEach($scope.records, function(record,k) { 
				if(angular.equals(record.data,n.data)){

					if(record.costs > n.costs) $scope.clon.push([n,record]); // Coste Uniforme
					else $scope.clon.push([record,n]);
					// if(record.level > n.level) $scope.clon.push([n,record]); // Coste Uniforme
					// else $scope.clon.push([record,n]);
					if( !(record.lvl >= $scope.max && n.level < $scope.max) ){
						$scope.records.push(n);
						return 0;
					}
				}
			});
			$scope.records.push(n);
			moveHole(ny,nx,n);
			node.child.push(n);
		}
	}
	// ----------------------------------------------------------------------BUSQUEDA
	var stack = [];
	var temp = {};
	var free = false;
	$scope.tope = 'na';
	$scope.isGrafo;
	$scope.profundidad = false;

	$scope.search = function(method){
		$scope.tope = 0;
		if(tree.child.length >= 0){		
			$scope.fin = false;
			$scope.routes = [];
			$scope.way = [];
			$scope.profundidad = false;
			$scope.button = 'enabled';
			stack = [];
			angular.copy(tree,temp);
			switch(method){
				case 'preOrden': preOrden(temp);
				break;
				case 'inOrden': inOrden(temp);
				break;
				case 'postOrden': postOrden(temp);
				break;
				case 'amplitud': amplitud(temp);
				break;
				case 'profundidad': profundidad(temp);
				break;
				case 'profundidadIt': profundidadIt(temp);
				break;
				case 'costeUniforme': costeUniforme(temp);
				break;
				case 'bestFirst': bestFirst(temp);
				break;
				case 'colina': AC(temp);
				break;
				case 'A': A(temp);
				break;
				case 'gameFree': free = !free;
				break;
			}
		} else { alert('Primero debes crear el arbol'); }
	}
	var preOrden = function(node){
		if($scope.fin == false){
			$scope.routes.push(node);
			if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true;
			else if(node.child.length>0){
				angular.forEach(node.child, function(n,k) { if($scope.fin == false) preOrden(n);	});
			}
		}
	}
	var postOrden = function(node){
		if($scope.fin == false){
			if(node.child.length>0) angular.forEach(node.child, function(n,k) { postOrden(n); });
			if($scope.fin == false) $scope.routes.push(node);
			if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true;
		}
	}
	var inOrden = function(node){
		if($scope.fin == false){
			if(node.child.length>0){
				angular.forEach(node.child, function(n,k) {
					if(k<=node.child.length) inOrden(n);
					if(node.visited==false){
						$scope.routes.push(node);
						node.visited = true;
					}
					if(k>node.child.length) inOrden(n);
				});
			}
			if($scope.fin == false && node.visited==false) $scope.routes.push(node);
			if(angular.equals(node.data,$scope.meta)==true){
				$scope.routes.push(node);
				$scope.fin = true;
			}
		}
	}
	var amplitud = function(tree){
		stack = [tree];
		expand();
		$scope.routes = getRoute(stack[stack.length-1]);
	}
	var expand = function(){
		angular.forEach(stack, function(n,k){
			if(n.visited==false && n.child.length>0){			
				angular.forEach(n.child, function(c,k) {
					if($scope.fin==false){
						$scope.way.push(c.node);
						if(angular.equals(c.data,$scope.meta)==true) $scope.fin=true;
						stack.push(c);
					}
				});
				n.visited = true;
				expand();
			}
		});
	}
	var profundidad = function(node){
		$scope.routes.push(node);
		$scope.way.push(node.node);
		node.visited = true;
		if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true;
		else if(node.child.length>0){
			angular.forEach(node.child, function(n,k) {
				if($scope.fin == false) profundidad(n);
				if($scope.fin == false) delete $scope.routes[n];
			});
		}
	}
	
	var profundidad2 = function(node){
		if(node.visited == false){
			$scope.way.push(node.node);
			node.visited = true;
		}
		if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true;
		else if(node.level > $scope.tope) stack.push(node);
		else if(node.child.length>0){ angular.forEach(node.child, function(n,k) { if($scope.fin == false) profundidad2(n); }); }
	}
	var profundidadIt = function(node){
		$scope.profundidad = true;
		$scope.tope = 1;
		profundidad2(node);
	}
	$scope.addLvl = function(node){
		angular.element('span.way:last-child').css('text-decoration','underline');
		$scope.tope = $scope.tope + 2;
		angular.forEach(stack, function(n,k) { profundidad2(n); });
		if($scope.fin == true){
			$scope.routes = getRoute(stack[stack.length-1]);
			$scope.button = 'disabled';
		}
	}
	var costeUniforme = function(tree){
		stack = [tree];
		expand();
		$scope.routes = getRoute(stack[stack.length-1]);
		searchRoutes($scope.routes);
	}
	var bestFirst = function(node){
		$scope.routes.push(node); // apila los nodos que se han analizado
		$scope.way.push(node.node); // registra la ruta que se ha llevado
		node.visited = true; // setea el nodo como visitado
		if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true; // checa si se llegó a la meta
		else if(node.child.length>0){ // evalúa que el nodo no se haya visitado antes y que tenga hijos
			node.child.sort(function(a,b) {	// ordena los hijos del nodo según la cantidad de coincidencias con la meta de mayor a menor
				if(a.coinc > b.coinc) return -1;
				if(a.coinc < b.coinc) return 1;
				return 0;
			});
			angular.forEach(node.child, function(n,k) { // se realiza profundidad pero dando prioridad a los nodos con más coincidencias
				if($scope.fin == false) bestFirst(n);	// realiza el mismo procedimiento con los hijos desde el mejor hasta el peor
				if($scope.fin == false) delete $scope.routes[n];	// si se registró el nodo y eventualmente sus hijos
			}); // y no se encontró la meta, entonces no es la ruta y se elimina
		}

	}
	var AC = function(node){
		node.visited = true;
		$scope.way.push(node.node);
		if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true;
		else if(node.child.length>0){
			node.child.sort(function(a,b) {
				if(a.coinc > b.coinc) return -1;
				if(a.coinc < b.coinc) return 1;
				return 0;
			});
			angular.forEach(node.child, function(c,k) { if($scope.fin==false) stack.push(c); });
			for(n=0; n<node.child.length && $scope.fin==false; n++){
				angular.forEach(stack, function(s,k) {
					if(node.child[n]==s && node.child[n].visited==false){
						if($scope.fin==false) AC(node.child[n]);
						$scope.fin=true;
					}
				});
			}if($scope.fin==false) angular.forEach(stack, function(s,k) { if(s.visited==false) AC(s); });
		}$scope.routes = getRoute(stack[stack.length-1]);
	}
	var A = function(node){

		$scope.routes.push(node); // apila los nodos que se han analizado
		$scope.way.push(node.node); // registra la ruta que se ha llevado
		node.visited = true; // setea el nodo como visitado
		if(angular.equals(node.data,$scope.meta)==true) $scope.fin = true; // checa si se llegó a la meta
		else if(node.child.length>0){ // evalúa que el nodo no se haya visitado antes y que tenga hijos
			
			angular.forEach(node.child, function(n,k) {
				if(angular.equals(n.data,$scope.meta)==true) $scope.fin = true;
				else if(n.child.length>0){
					angular.forEach(n.child, function(nn,kn) {
						stack.push(nn);
					});
				}
			});
			stack.sort(function(a,b) {	// ordena los hijos del nodo según la cantidad de coincidencias con la meta de mayor a menor
				if(a.cost > b.cost) return -1;
				if(a.cost < b.cost) return 1;
				return 0;
			});
			console.log(stack);
			angular.forEach(stack, function(n,k) { // se realiza profundidad pero dando prioridad a los nodos con más coincidencias
				if($scope.fin == false) bestFirst(n);	// realiza el mismo procedimiento con los hijos desde el mejor hasta el peor
				if($scope.fin == false) delete $scope.routes[n];	// si se registró el nodo y eventualmente sus hijos
			}); // y no se encontró la meta, entonces no es la ruta y se elimina
		}
		$scope.routes = getRoute($scope.routes[$scope.routes.length-1]);

	}
	$scope.moveKey = function(y,x){
		
		if(free==true){		
			var n = {data:[]}
			if($scope.routes.length == 0){
				angular.copy($scope.matrix,n.data);
				$scope.routes.push(n);
			}
			if($scope.fin==false){
				for(my=y-1; my<=y+1; my++) {
					if(my>-1 && my<3){
						for(mx=x-1; mx<=x+1; mx++) {
							if($scope.matrix[my][mx]==0 && (mx==x||my==y)) {
								$scope.matrix[my][mx] = $scope.matrix[y][x];
								$scope.matrix[y][x] = 0;
								angular.copy($scope.matrix,n.data);
								$scope.routes.push(n);
								if(angular.equals(n.data,$scope.meta)==true) $scope.fin = true;
							}
						}
					}
				};
			}
		}
		
	}
	$scope.reproducir = function(){

		let promise = $timeout();
		angular.forEach($scope.routes, function(value, key) {
			promise = promise.then(function() {
				angular.copy(value.data,$scope.matrix);
				return $timeout(500);
			});
		});
	}
	var searchRoutes = function(node){
		angular.forEach(node, function(n,kn) {
			$scope.way.push(n.node);
			angular.forEach($scope.clon, function(c,kc) {
				if(angular.equals(n.node,c[1].node)==true){
					$scope.routes.splice(0, kn+1);
					$scope.routes.push.apply($scope.routes,getRoute(c[0]));
					$scope.way.pop();
					$scope.way.push(c[1].node);
					searchRoutes($scope.routes);
				}
			});
		});
	}
	var getRoute = function(n){
		var routes = [];
		var node = {};
		angular.copy(n,node);
		while(node.level>=0){
			var pushed = {};
			angular.copy(node,pushed);
			routes.unshift(pushed);
			angular.copy(node.father,node);
		}
		return routes;
	}
	$scope.create();
});