var templateApp = angular.module('templateApp',[
    'ngAnimate',
    'ngMessages',
    'ngResource',
    'ngCookies',
    'ngSanitize',
])